import scala.annotation.tailrec

object Project extends App {

  case class Feedback(rating: Double, review: String) // use of case class

  case class Employee(name: String, position: String, noOfMonthsInPosition: Int) // use of case class

  val survey: Map[Employee, Feedback] =
    Map(
      Employee("John", "SE", 12) -> Feedback(3.5, "OK"),
      Employee("Joe", "HR", 3) -> Feedback(4.0, "Nice place to work"),
      Employee("Finn", "SE", 10) -> Feedback(5.0, "Awesome"),
      Employee("Stuart", "BA", 6) -> Feedback(2.0, "Worst")
    )

  /**
   * Ratings above the cutoff rating are marked as happy.
   */
  def isHappy(feedback: Feedback, cutoff: Double = 3.7): Boolean = feedback.rating > cutoff

  /**
   * Filter outs the happy employees, calling the isHappy method above
   */
  def happyEmployees(survey: Map[Employee, Feedback]): List[Employee] = {
    survey.filter(e => isHappy(e._2)).keys.toList // use of higher order function
  }

  /**
   * Filter outs the sad employees, calling the isHappy method above
   */
  def sadEmployees(survey: Map[Employee, Feedback]): List[Employee] = {
    survey.filterNot(e => isHappy(e._2)).keys.toList // use of higher order function
  }

  /**
   * Take the values list which is Feedback list of the survey map and iterates through it and filter
   * out the sad employees. Then the returned list is mapped to return only the reviews
   */
  def commentsOfSadEmployees(survey: Map[Employee, Feedback]): List[String] = {
    survey.values
      .filter(!isHappy(_))
      .map(_.review)
      .toList // use of higher order function
  }

  println(happyEmployees(survey))
  println(sadEmployees(survey))
  println(commentsOfSadEmployees(survey))

  /**
   * First checks the survey map is empty. If it's empty it returns the default empty map. Else, it takes the head
   * and take the position of head node. At the start empPos Map is empty and from the getOrElse method,
   * it returns 0 in the first attempt as it doesn't contain any value for the position. Then it creates
   * a new Map from the existing map by adding "count + 1" for the current position. Then, call the recursive call with
   * the tail.
   */
  @tailrec
  def numberOfEmployeesVotedWithPositions(survey: Map[Employee, Feedback], empPos: Map[String, Int] = Map[String, Int]()): Map[String, Int] = {
    // Recursion that terminates
    if (survey.isEmpty) empPos
    else {
      val position = survey.head._1.position
      val currentCount = empPos.getOrElse(position, 0)
      val newEmpPos = empPos + (position -> (currentCount + 1))

      numberOfEmployeesVotedWithPositions(survey.tail, newEmpPos)
    }

  }

  @tailrec
  def allRatingsBasedOnPositions(survey: Map[Employee, Feedback], positionRatings: Map[String, List[Double]] = Map()): Map[String, List[Double]] = {
    // Recursion that terminates
    if (survey.isEmpty) positionRatings
    else {
      val position = survey.head._1.position
      val currentList = positionRatings.getOrElse(position, List[Double]())
      val newList = currentList :+ survey.head._2.rating
      allRatingsBasedOnPositions(survey.tail, positionRatings + (position -> newList))
    }
  }

  println(numberOfEmployeesVotedWithPositions(survey))
  println(allRatingsBasedOnPositions(survey))

  /**
   *
   * @param survey
   * @param cutoff
   * @param months
   * @return This method gives the happy employees after the first call. We can further sort out
   *         the happy employees after a given months of working.
   */
  def employeeEmotionsOnMonths(fn: Map[Employee, Feedback] => List[Employee], survey: Map[Employee, Feedback])(months: Int): List[Employee] = {
    fn(survey).filter(_.noOfMonthsInPosition > months)
  }

  val happyEmps: Int => List[Employee] = employeeEmotionsOnMonths(happyEmployees, survey)
  val sadEmps: Int => List[Employee] = employeeEmotionsOnMonths(sadEmployees, survey)

  def employeeEmotionsOnPositions(fn: Map[Employee, Feedback] => List[Employee], survey: Map[Employee, Feedback])(position: String): List[Employee] = {
    fn(survey).filter(_.position == position)
  }

  val happySE: String => List[Employee] = employeeEmotionsOnPositions(happyEmployees, survey)
  val sadBA: String => List[Employee] = employeeEmotionsOnPositions(sadEmployees, survey)

  println(happyEmps(6)) // partial application
  println(sadEmps(6))

  println(happySE("SE")) // partial application
  println(sadBA("BA"))

  def sendCustomizedThankYouMessages(survey: Map[Employee, Feedback]) = {
    // pattern matching
    survey.keys.map { e =>
      e.position match {
        case "SE" => println(s"Thank you ${e.name} for working hardly for our company as a Software Engineer!")
        case "BA" => println(s"Thank you ${e.name} for working hardly for our company as a Business Analyst!")
        case "HR" => println(s"Thank you ${e.name} for working hardly for our company as a Human Resources Manager!")
        case _ => println(s"Thank you ${e.name} for working hardly for our company!")
      }
    }
  }

  def categorizeSeniority(survey: Map[Employee, Feedback]) = {
    // pattern matching
    survey.keys.map {
      case Employee(name, _, noOfMonthsInPosition) if noOfMonthsInPosition <= 6 =>
        s"Employee $name voted as a junior"
      case Employee(name, _, _) =>
        s"Employee $name voted as a senior"
    }.toList
  }

  sendCustomizedThankYouMessages(survey)
  println(categorizeSeniority(survey))

}
